package com.rednavis.pdf.itext;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import com.rednavis.pdf.itext.entity.User;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class PdfUserReportService implements UserReportService {

    private static final List<String> HEADER_LIST = Arrays.asList(
            "IMDB",
            "YEAR",
            "TITLE",
            "DIRECTOR(s)",
            "COUNTRY",
            "DURATION"
    );

    @Override
    public void createUserTableReport(File file, List<? extends User> usersList) throws IOException {
        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(new PdfWriter(file));

        // Initialize document
        Document document = new Document(pdf, PageSize.A4.rotate());
        Table table = new Table(new float[]{3, 2, 14, 9, 4, 3});
        table.setWidthPercent(100);

        HEADER_LIST.forEach(table::addHeaderCell);

        Cell cell = new Cell(1, 6).add("Continued on next page...");
        table.addFooterCell(cell)
                .setSkipLastFooter(true);

        usersList.forEach(user -> {
            table.addCell(user.getImdb());
            table.addCell(user.getYear());
            table.addCell(user.getTitle());
            table.addCell(user.getDirector());
            table.addCell(user.getCountry());
            table.addCell(user.getDuration());
        });
        document.add(table);
        document.close();
    }
}

