package com.rednavis.pdf.itext;

import com.rednavis.pdf.itext.entity.User;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Eugene Tsydzik
 * on 5/12/18.
 */
public interface UserReportService {

    void createUserTableReport(File file, List<? extends User> usersList) throws IOException;
}
