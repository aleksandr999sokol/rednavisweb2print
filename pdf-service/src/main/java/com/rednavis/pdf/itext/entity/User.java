package com.rednavis.pdf.itext.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Eugene Tsydzik
 * on 5/12/18.
 */
@Data
@AllArgsConstructor
public class User {

    private String imdb;
    private String year;
    private String title;
    private String director;
    private String country;
    private String duration;
}
