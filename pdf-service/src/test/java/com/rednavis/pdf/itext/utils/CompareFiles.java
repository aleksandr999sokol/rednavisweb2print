package com.rednavis.pdf.itext.utils;

import com.testautomationguru.utility.PDFUtil;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Created by Eugene Tsydzik
 * on 5/12/18.
 */
public class CompareFiles {

    public static boolean filesEquals(File file, String resourcePath) throws IOException {
        File expectedTempFile = Files.createTempFile(null, ".pdf").toFile();

        try (InputStream resourceAsStream = CompareFiles.class.getResourceAsStream(resourcePath);
             FileOutputStream fileOutputStream = new FileOutputStream(expectedTempFile)) {
            IOUtils.copy(resourceAsStream, fileOutputStream);

            return new PDFUtil().compare(file.getAbsolutePath(), expectedTempFile.getAbsolutePath());
        }
    }
}
