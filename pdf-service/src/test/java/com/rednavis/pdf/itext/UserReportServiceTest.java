package com.rednavis.pdf.itext;

import com.rednavis.pdf.itext.entity.User;
import com.rednavis.pdf.itext.utils.CompareFiles;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Eugene Tsydzik
 * on 5/12/18.
 */
public class UserReportServiceTest {

    private UserReportService userReportService = new PdfUserReportService();

    @Test
    public void testUserTableReport() throws IOException {
        User user1 = new User("0126875", "1908", "Dr. Jekyll and Mr. Hyde", "Otis Turner", "USA", "16");
        User user2 = new User("0154614", "1915", "Horrible Hyde", "Rouben Mamoulian", "RUS", "11");
        User user3 = new User("0011130", "1928", "Dr. Jekyll", "William Vance", "BEL", "13");
        User user4 = new User("0011131", "1938", "Dr. Gentleman", "Victor Fleming", "UKR", "22");
        User user5 = new User("0011348", "1948", "Dr. Strange", "Joseph Barbera", "POL", "10");
        List<User> usersList = Arrays.asList(user1, user2, user3, user4, user5);

        File file = Files.createTempFile("userReport-", ".pdf").toFile();
        userReportService.createUserTableReport(file, usersList);

        assertTrue(CompareFiles.filesEquals(file, "/reports/userTableReport.pdf"));
    }
}
